﻿using HealthYou.Classes;
using HealthYou.Classes.Interfaces;
using HealthYou.Classes.Logic;
using HealthYou.Classes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthYou
{
    /// <summary>
    /// Логика взаимодействия для AmountOfActivitiesDoneWindow.xaml
    /// </summary>
    public partial class AmountOfActivitiesDoneWindow : Window
    {
		IRepository _repo = Factory.Instance.GetRepository();
		IProgram _prog = new Program();

		public AmountOfActivitiesDoneWindow()
        {
            InitializeComponent();
            ActivitiesDataGrid.ItemsSource = _repo.User.CurrentCondition.DoneExercises;
        }

		private void UpdateDataGrid()
		{
			ActivitiesDataGrid.ItemsSource = null;
			ActivitiesDataGrid.ItemsSource = _repo.User.CurrentCondition.DoneExercises;
		}

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            var addingActivitiesWindow = new AddingActivitiesWindow();
            addingActivitiesWindow.Show();
            this.Close();
        }

        private void ButtonRemove_Click(object sender, RoutedEventArgs e)
        {
            if (ActivitiesDataGrid.SelectedItem != null)
            {
                _prog.RemoveActivity(ActivitiesDataGrid.SelectedItem as DoneExercise);
                _repo.Save();
				UpdateDataGrid();
            }
            else
                MessageBox.Show("Please, select an activity to delete");
        }

        private void ButtonOK_Click(object sender, RoutedEventArgs e)
        {
            var activityWindow = new ActivityWindow();
            activityWindow.Show();
            this.Close();
        }
    }
}
