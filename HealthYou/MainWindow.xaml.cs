﻿using HealthYou.Classes;
using HealthYou.Classes.Helpers;
using HealthYou.Classes.Interfaces;
using HealthYou.Classes.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HealthYou
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
		IRepository _repo = Factory.Instance.GetRepository();

		public MainWindow()
		{
			InitializeComponent();
		}

		private void ButtonLogIn_Click(object sender, RoutedEventArgs e)
		{
			_repo.Autorization(textBoxEmail.Text, PasswordHelper.GetHash(passwordBoxPassword.Password));

			if (_repo.User != null)
			{
				var todayWindow = new TodayWindow();
				todayWindow.Show();
				this.Close();
			}
			else
				MessageBox.Show("Incorrect email or password!");
		}

		private void ButtonSignUp_Click(object sender, RoutedEventArgs e)
		{
			var signUpWindow = new SignUpWindow();
			signUpWindow.Show();
			this.Close();
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{

		}
	}
}
