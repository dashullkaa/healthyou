﻿using HealthYou.Classes;
using HealthYou.Classes.Interfaces;
using HealthYou.Classes.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthYou
{
    /// <summary>
    /// Логика взаимодействия для ProfileWindow.xaml
    /// </summary>
    public partial class ProfileWindow : Window
    {
		IRepository _repo = Factory.Instance.GetRepository();
		IProgram _prog = new Program();

		public ProfileWindow()
		{
			InitializeComponent();
			_repo.Restore();
			GenderComboBox.ItemsSource = new List<String> { "male", "female" };
			GoalComboBox.ItemsSource = new List<String> { "Lose weight", "Become healthier", "Gain weight" };
			if (_repo.User.Age!=0)
				AgeTextBox.Text = _repo.User.Age.ToString();
			if (_repo.User.Height!=0)
				HeightTextBox.Text = _repo.User.Height.ToString();
			if (_repo.User.Weight!=0)
				WeightTextBox.Text = _repo.User.Weight.ToString();
			if (_repo.User.Gender!=null)
				GenderComboBox.SelectedItem = _repo.User.Gender;
			if (_repo.User.Goals!=null)
				GoalComboBox.SelectedItem = _repo.User.Goals.Name;
		}

		private void ButtonOK_Click(object sender, RoutedEventArgs e)
		{
			_repo.User.Age = int.Parse(AgeTextBox.Text);
			_repo.User.Gender = GenderComboBox.SelectedItem.ToString();
			_repo.User.Height = int.Parse(HeightTextBox.Text);
			_repo.User.Weight = int.Parse(WeightTextBox.Text);
			_prog.CountGoals(GoalComboBox.SelectedItem.ToString());
			_repo.Save();

			var todayWindow = new TodayWindow();
			todayWindow.Show();
			this.Close();
		}

		private void ButtonCancel_Click(object sender, RoutedEventArgs e)
		{
			var todayWindow = new TodayWindow();
			todayWindow.Show();
			this.Close();
		}
	}
}
