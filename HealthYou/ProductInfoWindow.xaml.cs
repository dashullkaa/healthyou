﻿using HealthYou.Classes;
using HealthYou.Classes.Interfaces;
using HealthYou.Classes.Logic;
using HealthYou.Classes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthYou
{
    /// <summary>
    /// Логика взаимодействия для ProductInfoWindow.xaml
    /// </summary>
    public partial class ProductInfoWindow : Window
    {
		IRepository _repo = Factory.Instance.GetRepository();
		IProgram _prog = new Program();
		public Product Product { get; set; }

        public ProductInfoWindow(Product product)
        {
            InitializeComponent();
            NameTextBlock.Text = product.Name;
            KCALTextBlock.Text = product.KcalPerGram.ToString();
            Product = product;
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            if (GramsTextBox.Text == null)
            {
                MessageBox.Show("Please, enter the amount of grams eaten");
            }

            else if (int.Parse(GramsTextBox.Text) < 0 || int.Parse(GramsTextBox.Text) == 0)
            {
                MessageBox.Show("This value can not be negative or zero");
            }

            else
            {
				_prog.AddProduct(Product, int.Parse(GramsTextBox.Text));
                _repo.Save();
                var amountOfEatenCaloriesToday = new AmountOfEatenCaloriesToday();
                amountOfEatenCaloriesToday.Show();
                this.Close();
            }  
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            var addingCaloriesWindow = new AddingCaloriesWindow();
            addingCaloriesWindow.Show();
            this.Close();
        }
    }
}
