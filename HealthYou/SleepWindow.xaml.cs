﻿using HealthYou.Classes;
using HealthYou.Classes.Interfaces;
using HealthYou.Classes.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthYou
{
    /// <summary>
    /// Логика взаимодействия для SleepWindow.xaml
    /// </summary>
    public partial class SleepWindow : Window
    {
		IRepository _repo = Factory.Instance.GetRepository();
		IProgram _prog = new Program();

		public SleepWindow()
        {
            InitializeComponent();
            SleepPB.Value = _prog.SleepingProgress();

            HoursTextBlock.Text = Math.Round(_repo.User.SleepingHours/60,1).ToString();
			PercentTextBlock.Text = _prog.SleepingProgress().ToString() + " %";
			if (_repo.User.SleepingHours < 480)
				texBlockNeedToSleep.Text = "You need to sleep " + Math.Round(((480 - _repo.User.SleepingHours)/60),1).ToString() + " hours more";
			else texBlockNeedToSleep.Text = "Sleeping rate fulfilled!";

		}

        private void ButtonBack_Click(object sender, RoutedEventArgs e)
        {
            var todayWindow = new TodayWindow();
            todayWindow.Show();
            this.Close();
        }

        private void ButtonEdit_Click(object sender, RoutedEventArgs e)
        {
            var amountOfSleepingHoursWindow = new AmountOfSleepingHoursWindow();
            amountOfSleepingHoursWindow.Show();
            this.Close();
        }
    }
}
