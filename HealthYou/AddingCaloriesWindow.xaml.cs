﻿using HealthYou.Classes;
using HealthYou.Classes.Interfaces;
using HealthYou.Classes.Logic;
using HealthYou.Classes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthYou
{
    /// <summary>
    /// Логика взаимодействия для AddingCaloriesWindow.xaml
    /// </summary>
    public partial class AddingCaloriesWindow : Window
    {
		IRepository _repo = Factory.Instance.GetRepository();
		IProgram _prog = new Program();

		public AddingCaloriesWindow()
        { 
            InitializeComponent();
			_repo.Restore();
        }

        private void ButtonSearch_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(SearchTextBox.Text))
            {
                _prog.SearchProduct(SearchTextBox.Text);
                if (_prog.SearchProduct(SearchTextBox.Text) != null)
                    SearchingFoodDataGrid.Items.Add(_prog.SearchProduct(SearchTextBox.Text));
                else
                    MessageBox.Show("There is no such product in our base, however you can add it yourself");
            }
            else
                MessageBox.Show("Please, enter the name of the product");
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            if (SearchingFoodDataGrid.SelectedItem == null)
                MessageBox.Show("Please, select the product to add");

            if (SearchingFoodDataGrid.SelectedItem != null)
            {
                var productInfoWindow = new ProductInfoWindow(SearchingFoodDataGrid.SelectedItem as Product);
                productInfoWindow.Show();
                this.Close();
            }
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            var kcalWindow = new KCALWindow();
            kcalWindow.Show();
            this.Close();
        }

        private void ButtonAddNew_Click(object sender, RoutedEventArgs e)
        {
            var newProductWindow = new NewProductWindow();
            newProductWindow.Show();
            this.Close();
        }
    }
}
