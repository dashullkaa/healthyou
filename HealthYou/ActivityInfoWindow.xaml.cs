﻿using HealthYou.Classes;
using HealthYou.Classes.Interfaces;
using HealthYou.Classes.Logic;
using HealthYou.Classes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthYou
{
    /// <summary>
    /// Логика взаимодействия для ActivityInfoWindow.xaml
    /// </summary>
    public partial class ActivityInfoWindow : Window
    {
		IRepository _repo = Factory.Instance.GetRepository();
		IProgram _prog = new Program();
		public Activity Activity { get; set; }

        public ActivityInfoWindow(Activity activity)
        {
            InitializeComponent();
            Activity = activity;
            NameTextBlock.Text = activity.Name;
            KCALTextBlock.Text = activity.BurningKcalsPerMinute.ToString();
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            if (MinutesTextBox.Text == null)
                MessageBox.Show("Please, enter the amount of done activities");
            else if (int.Parse(MinutesTextBox.Text) <= 0)
                MessageBox.Show("This value can not be negative or zero");
            else
            {
                _prog.AddActivity(Activity, int.Parse(MinutesTextBox.Text));
                _repo.Save();
                var AmountOfActivitiesDoneWindow = new AmountOfActivitiesDoneWindow();
                AmountOfActivitiesDoneWindow.Show();
                this.Close();
            }
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            var AmountOfActivitiesDoneWindow = new AmountOfActivitiesDoneWindow();
            AmountOfActivitiesDoneWindow.Show();
            this.Close();
        }
    }
}
