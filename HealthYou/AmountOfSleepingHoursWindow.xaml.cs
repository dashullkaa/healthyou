﻿using HealthYou.Classes;
using HealthYou.Classes.Interfaces;
using HealthYou.Classes.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthYou
{
    /// <summary>
    /// Логика взаимодействия для AmountOfSleepingHoursWindow.xaml
    /// </summary>
    public partial class AmountOfSleepingHoursWindow : Window
    {
		IRepository _repo = Factory.Instance.GetRepository();
		IProgram _prog = new Program();

		public AmountOfSleepingHoursWindow()
        {
            InitializeComponent();
        }

        private void ButtonOK_Click(object sender, RoutedEventArgs e)
        {
			if (!(StartHoursTextBox.Text == null || StartMinutesTextBox.Text == null|| FinishHoursTextBox.Text == null || FinishMinutesTextBox.Text == null))
			{
				if(int.Parse(StartHoursTextBox.Text) < 0 || int.Parse(StartMinutesTextBox.Text) < 0 
					|| int.Parse(FinishMinutesTextBox.Text) < 0 || int.Parse(FinishHoursTextBox.Text) < 0||
					int.Parse(StartMinutesTextBox.Text) > 59 || int.Parse(FinishMinutesTextBox.Text) > 59 ||
					int.Parse(StartHoursTextBox.Text)>23|| int.Parse(FinishHoursTextBox.Text)>23)
					MessageBox.Show("Incorrect time!");
				else
				{
					_prog.CountSleepingHours(int.Parse(StartHoursTextBox.Text), int.Parse(StartMinutesTextBox.Text), int.Parse(FinishHoursTextBox.Text), int.Parse(FinishMinutesTextBox.Text));
					_repo.Save();

					var sleepWindow = new SleepWindow();
					sleepWindow.Show();
					this.Close();
				}
			}

            else
            {
				MessageBox.Show("Please, enter the time of sleep");
			}
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            var sleepWindow = new SleepWindow();
            sleepWindow.Show();
            this.Close();
        }
    }
}
