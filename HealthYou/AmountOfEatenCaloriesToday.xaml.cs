﻿using HealthYou.Classes;
using HealthYou.Classes.Interfaces;
using HealthYou.Classes.Logic;
using HealthYou.Classes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthYou
{
    /// <summary>
    /// Логика взаимодействия для AmountOfEatenCaloriesToday.xaml
    /// </summary>
    public partial class AmountOfEatenCaloriesToday : Window
    {
		IRepository _repo = Factory.Instance.GetRepository();
		IProgram _prog = new Program();

		public AmountOfEatenCaloriesToday()
        {
            InitializeComponent();
			FoodDataGrid.ItemsSource = _repo.User.CurrentCondition.EatenProducts;
		}

		private void UpdateDataGrid()
		{
			FoodDataGrid.ItemsSource = null;
			FoodDataGrid.ItemsSource = _repo.User.CurrentCondition.EatenProducts;
		}

		private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            var addingCaloriesWindow = new AddingCaloriesWindow();
            addingCaloriesWindow.Show();
            this.Close();
        }

        private void ButtonRemove_Click(object sender, RoutedEventArgs e)
        {
            if (FoodDataGrid.SelectedItem != null)
            {
                _prog.RemoveProduct(FoodDataGrid.SelectedItem as EatenProduct);
                _repo.Save();
				UpdateDataGrid();

			}

            else
                MessageBox.Show("Please select a product to delete");
        }

        private void ButtonOK_Click(object sender, RoutedEventArgs e)
        {
            var kcalWindow = new KCALWindow();
            kcalWindow.Show();
            this.Close();
        }
    }
}
