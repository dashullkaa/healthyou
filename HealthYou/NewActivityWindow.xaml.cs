﻿using HealthYou.Classes;
using HealthYou.Classes.Interfaces;
using HealthYou.Classes.Logic;
using HealthYou.Classes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthYou
{
    /// <summary>
    /// Логика взаимодействия для NewActivityWindow.xaml
    /// </summary>
    public partial class NewActivityWindow : Window
    {
		IRepository _repo = Factory.Instance.GetRepository();
		IProgram _prog = new Program();

		public NewActivityWindow()
        {
            InitializeComponent();
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            if (NameTextBox.Text == null)
                MessageBox.Show("Please, enter the name of activity");
            else if (KCALTextBox.Text == null)
                MessageBox.Show("Please, enter the amount of calories burn in an hour");
            else if (int.Parse(KCALTextBox.Text) <= 0)
                MessageBox.Show("This value can not be negative or zero");

            else
            {
                Activity activity = new Activity()
                {
                    Name = NameTextBox.Text,
                    BurningKcalsPerMinute = double.Parse(KCALTextBox.Text)
                };

                _repo.AddNewActivity(activity);
                _repo.Save();

                var addingActivitiesWindow = new AddingActivitiesWindow();
                addingActivitiesWindow.Show();
                this.Close();
            }
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            var addingActivitiesWindow = new AddingActivitiesWindow();
            addingActivitiesWindow.Show();
            this.Close();
        }
    }
}
