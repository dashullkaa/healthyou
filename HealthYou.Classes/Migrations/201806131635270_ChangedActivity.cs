namespace HealthYou.Classes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedActivity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Activities", "KcalsBurnt", c => c.Double());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Activities", "KcalsBurnt");
        }
    }
}
