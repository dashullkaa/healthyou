namespace HealthYou.Classes.Migrations
{
	using HealthYou.Classes.Models;
	using Newtonsoft.Json;
	using System;
	using System.Collections.Generic;
	using System.Data.Entity;
    using System.Data.Entity.Migrations;
	using System.IO;
	using System.Linq;
	using System.Reflection;

	internal sealed class Configuration : DbMigrationsConfiguration<HealthYou.Classes.Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(HealthYou.Classes.Context context)
        {
			//  This method will be called after migrating to the latest version.

			//  You can use the DbSet<T>.AddOrUpdate() helper extension method 
			//  to avoid creating duplicate seed data.

			var resourseProducts = "HealthYou.Classes.Data.products.json";
			var resourseActivities = "HealthYou.Classes.Data.activities.json";

			var Products = RestoreList<Product>(resourseProducts);
			var Activities = RestoreList<Activity>(resourseActivities);

			foreach (var product in Products)
			{
				context.Products.AddOrUpdate(p => p.Name, product);
			}
			context.SaveChanges();
			foreach (var activity in Activities)
			{
				context.Activities.AddOrUpdate(a => a.Name, activity);
			}
			context.SaveChanges();
		}

		private List<T> RestoreList<T>(string resourseName)
		{
			var assembly = Assembly.GetExecutingAssembly();
			using (Stream stream = assembly.GetManifestResourceStream(resourseName))
			{
				using (var sr = new StreamReader(stream))
				{
					using (var jsonReader = new JsonTextReader(sr))
					{
						var serializer = new JsonSerializer();
						return serializer.Deserialize<List<T>>(jsonReader);
					}
				}
			}
		}
	}
}
