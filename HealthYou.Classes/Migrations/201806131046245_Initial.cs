namespace HealthYou.Classes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Activities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        BurningKcalsPerMinute = c.Double(nullable: false),
                        LeadTime = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        CurrentCondition_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Conditions", t => t.CurrentCondition_Id)
                .Index(t => t.CurrentCondition_Id);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200),
                        KcalPerGram = c.Double(nullable: false),
                        KcalsEaten = c.Double(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        CurrentCondition_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Conditions", t => t.CurrentCondition_Id)
                .Index(t => t.CurrentCondition_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FullName = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                        Age = c.Int(nullable: false),
                        Gender = c.String(),
                        Weight = c.Double(nullable: false),
                        Height = c.Int(nullable: false),
                        SleepingHours = c.Int(nullable: false),
                        CurrentCondition_Id = c.Int(),
                        Goals_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Conditions", t => t.CurrentCondition_Id)
                .ForeignKey("dbo.Conditions", t => t.Goals_Id)
                .Index(t => t.CurrentCondition_Id)
                .Index(t => t.Goals_Id);
            
            CreateTable(
                "dbo.Conditions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        KcalsEaten = c.Double(nullable: false),
                        ActivityLevel = c.Double(nullable: false),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "Goals_Id", "dbo.Conditions");
            DropForeignKey("dbo.Users", "CurrentCondition_Id", "dbo.Conditions");
            DropForeignKey("dbo.Products", "CurrentCondition_Id", "dbo.Conditions");
            DropForeignKey("dbo.Activities", "CurrentCondition_Id", "dbo.Conditions");
            DropIndex("dbo.Users", new[] { "Goals_Id" });
            DropIndex("dbo.Users", new[] { "CurrentCondition_Id" });
            DropIndex("dbo.Products", new[] { "CurrentCondition_Id" });
            DropIndex("dbo.Activities", new[] { "CurrentCondition_Id" });
            DropTable("dbo.Conditions");
            DropTable("dbo.Users");
            DropTable("dbo.Products");
            DropTable("dbo.Activities");
        }
    }
}
