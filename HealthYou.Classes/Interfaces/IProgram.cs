﻿using HealthYou.Classes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthYou.Classes.Interfaces
{
	public interface IProgram
	{
		void AddProduct(Product product, int grams);
		void RemoveProduct(EatenProduct product);
		void AddActivity(Activity activity, int time);
		void RemoveActivity(DoneExercise activity);
		void CountSleepingHours(int startHour, int startMinutes, int finishHour, int finishMinutes);
		void CountGoals(string name);
		Product SearchProduct(string name);
		Activity SearchActivity(string name);
		double ActivityProgress();
		double CaloriesProgress();
		double SleepingProgress();

	}
}
