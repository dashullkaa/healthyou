﻿using HealthYou.Classes.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthYou.Classes
{
	public class Context : DbContext
	{

		public Context()
			: base("HealthYouDb")
		{
		}

		public DbSet<Activity> Activities { get; set; }
		public DbSet<User> Users { get; set; }
		public DbSet<Product> Products { get; set; }

	}
}
