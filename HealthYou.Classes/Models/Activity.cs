﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthYou.Classes.Models
{
	public class Activity
	{
		[JsonIgnore]
		public int Id { get; set; }
		public string Name { get; set; }
		public double BurningKcalsPerMinute { get; set; }
	}
}
