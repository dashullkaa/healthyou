﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthYou.Classes.Models
{
	public class DoneExercise : Activity
	{
		public int LeadTime { get; set; }
		public double KcalsBurnt { get; set; }
	}
}
