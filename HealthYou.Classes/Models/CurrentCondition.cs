﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthYou.Classes.Models
{
	public class CurrentCondition : Condition
	{

		public List<EatenProduct> EatenProducts { get; set; }
		public List<DoneExercise> DoneExercises { get; set; }
	}
}
