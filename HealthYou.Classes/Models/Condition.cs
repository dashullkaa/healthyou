﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthYou.Classes.Models
{
	public class Condition
	{
		[JsonIgnore]
		public int Id { get; set; }
		public string Name { get; set; }
		public double KcalsEaten { get; set; }
		public double ActivityLevel { get; set; }
	}
}
