﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthYou.Classes.Models
{
	public class EatenProduct : Product
	{
		public double KcalsEaten { get; set; }
	}
}
