﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthYou.Classes.Models
{
	public class User
	{
		[JsonIgnore]
		public int Id { get; set; }
		public string FullName { get; set; }
		public string Email { get; set; }
		public string Password { get; set; }
		public int Age { get; set; }
		public string Gender { get; set; }
		public double Weight { get; set; }
		public int Height { get; set; }
		public double SleepingHours { get; set; }
		public Condition Goals { get; set; }
		public CurrentCondition CurrentCondition { get; set; }
	}
}
