﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthYou.Classes.Models
{
	public class Product
	{
		[JsonIgnore]
		public int Id { get; set; }
		[MaxLength(200)]
		public string Name { get; set; }
		public double KcalPerGram { get; set; }
	}
}
